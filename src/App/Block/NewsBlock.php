<?php

namespace App\Block;

use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;

class NewsBlock extends BaseBlockService
{
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse('App:NewsBlock:list.html.twig', [
            'block' => $blockContext->getBlock(),
        ], $response);
    }

}